#include <math.h>
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

Adafruit_PWMServoDriver pwm1 = Adafruit_PWMServoDriver();
Adafruit_PWMServoDriver pwm2 = Adafruit_PWMServoDriver(0x41);

#define servoMin 110  // Pulse น้อยสุด กำหนด 130 จาก 4096 Angle=0
#define servoMax 500  // Pulse มากสุด กำหนด 545 จาก 4096 Angle=180
unsigned long previousMillis = 0;
unsigned long interval = 20;  //use 3000 for three seconds on

double H[29];
double FL[4];
double FR[4];
double CL[4];
double CR[4];
double TL[4];
double TR[4];
double output[29];
double O_FL[4];
double O_FR[4];
double O_CL[4];
double O_CR[4];
double O_TL[4];
double O_TR[4];
double M_FL[4];
double M_FR[4];
double M_CL[4];
double M_CR[4];
double M_TL[4];
double M_TR[4];
double I[6];
double Mi = 0;
double A = 1.7246;
double B = -2.48285;
double C = -1.7246;
double MaxT = 0;
double MaxC = 0;
double MaxF = 0;

float state_1 = 0.2;   // Initial state
float state_2 = -0.4;  // Initial state

double MinT = 200;
double MinC = 200;
double MinF = 200;

double W_I1ToFL = 10;
double W_I2ToFL = -2;
double BToFL = -2;
double BToTL = -1;
double alpha = 1.15;
double phi = 3.1415926 / 28;
double w[2];
//double w[2] = { alpha * cos(phi), alpha* sin(phi) + Mi };
const int trig1 = 13;
int echo1 = 12;
int echo2 = 11;
int trig2 = 10;
long duration, cm;
//psn
double w_h3, w_h1, w_h4, w_h2, w_h5678, B_9101112, w_h9_, B_13_, W_h13, B_F, B_C;
//vrn
double w_h14, w_I4_5, w_27_28, w_T;

int count = 0;
String Menu, S_Mi, I1, I2, I3;
String Mi_1;
double s1, s2;
void setup() {
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pwm1.begin();
  pwm1.setPWMFreq(50);  // กำหนด Frequency = 60 Hz (เซอร์โวส่วนใหญ่จะทำงานที่ 50-60 Hz)
  pwm2.begin();
  pwm2.setPWMFreq(50);
  //psn
  I[1] = 0;
  I[2] = 1;
  I[3] = 1;
  w_h3 = -5;
  w_h1 = 0.5;
  w_h4 = -5;
  w_h2 = 0.5;
  w_h5678 = 0.5;
  B_9101112 = 0.5;
  w_h9_ = 3;
  B_13_ = -1.35;
  W_h13 = 2.2;
  B_F = -2;
  B_C = -1;

  //VRn
  w_h14 = 1.75;
  w_I4_5 = 5;
  w_27_28 = 0.5;
  w_T = 1.3;
  H[1] = { 0.5 };
  H[2] = { 0.5 };


  output[1] = tanh(H[1]);
  output[2] = tanh(H[2]);

  Serial.begin(115200);
  //set servo
  for (int i = 0; i < 12; i++) {
    if (i < 4) {
      pwm2.setPWM(i, 0, angleToPulse(180));
      pwm1.setPWM(i, 0, angleToPulse(0));
      // Serial.println("i 5 on,");
    } else if (i < 8) {
      pwm1.setPWM(i, 0, angleToPulse(180));
      pwm2.setPWM(i, 0, angleToPulse(0));
      //Serial.println("i 8 on,");
    } else {

      pwm1.setPWM(i, 0, angleToPulse(120));
      pwm2.setPWM(i, 0, angleToPulse(60));
      //Serial.println("i 10 on");
    }
  }
  delay(2000);
}

void loop() {
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;
    int Sensor1 = analogRead(A0);  //Left
    int Sensor2 = analogRead(A1);  //Right
    I[4] = 1;
    I[5] = 1;




    if (Sensor1 < 100) {
      I[5] = 1;
    }
    if (Sensor1 >= 100) {
      I[5] = -1;
    }
    if (Sensor2 < 100) {
      I[4] = 1;
    }
    if (Sensor2 >= 100) {
      I[4] = -1;
    }


    while (Serial.available()) {  // ปรับ Mi ผ่าน Serial
      Menu = Serial.readString();
      Menu.trim();
      if (Menu == "Mi_1") {
        Mi = 0.0;
      } else if (Menu == "Mi_2") {
        Mi = 0.2;
      } else if (Menu == "Mi_3") {
        Mi = 0.3;
      }
    }





    double w[2] = { 1.5, 0.4 + Mi };
    //double w[2] = { alpha * cos(phi), alpha * sin(phi) + Mi };
    
    //cpg
    H[1] = (output[2] * w[1]) + (output[1] * w[0]) + 0.01;
    H[2] = (output[1] * (-w[1])) + (output[2] * w[0]) + 0.01;
    output[1] = tanh(H[1]);
    output[2] = tanh(H[2]);



    //post cpg
    if (output[1] > -0.46) {
      // Result if condition 1 is true
      state_1 = state_1 - 0.089;
    } else if (output[1] < 0) {
      // Result if condition 2 is true
      state_1 = state_1 + 0.15;
    } else {
      // Result if neither condition 1 nor condition 2 is true
      state_1 = state_1;
      //state_2 = state_2;
    }

    if (output[2] > -0.5) {
      // Result if condition 1 is true
      state_2 = state_2 - 0.097;
    } else if (output[2] < 0) {
      // Result if condition 2 is true
      state_2 = state_2 + 0.16;
    } else {
      // Result if neither condition 1 nor condition 2 is true
      state_2 = state_2;
    }

   

    //psn
    H[3] = (I[3] * (-1)) + 1;
    H[4] = (I[3] * 1);

    //post-cpg
    // H[5] = (state_1 * w_h1) + (output[3] * w_h3);
    // H[6] = (state_2 * w_h2) + (output[4] * w_h4);
    // H[7] = (state_2 * w_h2) + (output[3] * w_h3);
    // H[8] = (state_1 * w_h1) + (output[4] * w_h4);

    H[5] = (output[1] * w_h1) + (output[3] * w_h3);
    H[6] = (output[2] * w_h2) + (output[4] * w_h4);
    H[7] = (output[2] * w_h2) + (output[3] * w_h4);
    H[8] = (output[1] * w_h1) + (output[4] * w_h3);

    H[9] = (output[5] * 0.5) + 0.5;
    H[10] = (output[6] * 0.5) + 0.5;
    H[11] = (output[7] * 0.5) + 0.5;
    H[12] = (output[8] * 0.5) + 0.5;

    H[13] = (output[9] * w_h9_) + (output[10] * w_h9_) + B_13_;
    H[14] = (output[11] * w_h9_) + (output[12] * w_h9_) + B_13_;


    //vrn
    H[15] = (output[14] * w_h14);
    H[16] = (I[4] * w_I4_5);
    H[17] = (output[14] * w_h14);
    H[18] = (I[5] * w_I4_5);
    H[19] = (output[15] * A) + (output[16] * A) + B;
    H[20] = (output[15] * C) + (output[16] * C) + B;
    H[21] = (output[15] * A) + (output[16] * C) + B;
    H[22] = (output[15] * C) + (output[16] * A) + B;
    H[23] = (output[17] * A) + (output[18] * A) + B;
    H[24] = (output[17] * C) + (output[18] * C) + B;
    H[25] = (output[17] * A) + (output[18] * C) + B;
    H[26] = (output[17] * C) + (output[18] * A) + B;
    H[27] = (output[19] * w_27_28) + (output[20] * w_27_28) + (output[21] * (-w_27_28)) + (output[22] * (-w_27_28));
    H[28] = (output[23] * w_27_28) + (output[24] * w_27_28) + (output[25] * (-w_27_28)) + (output[26] * (-w_27_28));


    output[3] = tanh(H[3]);
    output[4] = tanh(H[4]);
    output[5] = tanh(H[5]);
    output[6] = tanh(H[6]);
    output[7] = tanh(H[7]);
    output[8] = tanh(H[8]);
    output[9] = tanh(H[9]);
    output[10] = tanh(H[10]);
    output[11] = tanh(H[11]);
    output[12] = tanh(H[12]);
    output[13] = tanh(H[13]);
    output[14] = tanh(H[14]);
    output[15] = tanh(H[15]);
    output[16] = tanh(H[16]);
    output[17] = tanh(H[17]);
    output[18] = tanh(H[18]);
    output[19] = tanh(H[19]);
    output[20] = tanh(H[20]);
    output[21] = tanh(H[21]);
    output[22] = tanh(H[22]);
    output[23] = tanh(H[23]);
    output[24] = tanh(H[24]);
    output[25] = tanh(H[25]);
    output[26] = tanh(H[26]);
    output[27] = tanh(H[27]);
    output[28] = tanh(H[28]);
    
    //servo 
    FL[0] = (output[13] * (W_h13)) + (I[1] * W_I1ToFL) - (I[2] * W_I2ToFL) + B_F;
    FL[1] = (output[13] * (-W_h13)) + (I[1] * W_I1ToFL) - (I[2] * W_I2ToFL) + B_F;
    FL[2] = (output[13] * (W_h13)) + (I[1] * W_I1ToFL) - (I[2] * W_I2ToFL) + B_F;
    FL[3] = (output[13] * (-W_h13)) + (I[1] * W_I1ToFL) - (I[2] * W_I2ToFL) + B_F;

    FR[0] = (output[13] * (-W_h13)) + (I[1] * W_I1ToFL) - (I[2] * W_I2ToFL) + B_F;
    FR[1] = (output[13] * (W_h13)) + (I[1] * W_I1ToFL) - (I[2] * W_I2ToFL) + B_F;
    FR[2] = (output[13] * (-W_h13)) + (I[1] * W_I1ToFL) - (I[2] * W_I2ToFL) + B_F;
    FR[3] = (output[13] * (W_h13)) + (I[1] * W_I1ToFL) - (I[2] * W_I2ToFL) + B_F;

    CL[0] = (output[14] * (-W_h13)) + (I[1] * W_I1ToFL) + B_C;
    CL[1] = (output[14] * (W_h13)) + (I[1] * W_I1ToFL) + B_C;
    CL[2] = (output[14] * (-W_h13)) + (I[1] * W_I1ToFL) + B_C;
    CL[3] = (output[14] * (W_h13)) + (I[1] * W_I1ToFL) + B_C;

    CR[0] = (output[14] * (W_h13)) + (I[1] * W_I1ToFL) + B_C;
    CR[1] = (output[14] * (-W_h13)) + (I[1] * W_I1ToFL) + B_C;
    CR[2] = (output[14] * (W_h13)) + (I[1] * W_I1ToFL) + B_C;
    CR[3] = (output[14] * (-W_h13)) + (I[1] * W_I1ToFL) + B_C;

    TL[0] = (output[27] * (w_T)) + (I[1] * W_I1ToFL);
    TL[1] = (output[27] * (-w_T)) + (I[1] * (-W_I1ToFL));
    TL[2] = (output[27] * (w_T)) + (I[1] * (-W_I1ToFL));
    TL[3] = (output[27] * (-w_T)) + (I[1] * (-W_I1ToFL));

    TR[0] = (output[28] * (-w_T)) + (I[1] * W_I1ToFL);
    TR[1] = (output[28] * (w_T)) + (I[1] * (-W_I1ToFL));
    TR[2] = (output[28] * (-w_T)) + (I[1] * (-W_I1ToFL));
    TR[3] = (output[28] * (w_T)) + (I[1] * (-W_I1ToFL));

    //output servo
    O_FL[0] = tanh(FL[0]);
    O_FL[1] = tanh(FL[1]);
    O_FL[2] = tanh(FL[2]);
    O_FL[3] = tanh(FL[3]);


    O_FR[0] = tanh(FR[0]);
    O_FR[1] = tanh(FR[1]);
    O_FR[2] = tanh(FR[2]);
    O_FR[3] = tanh(FR[3]);

    O_CL[0] = tanh(CL[0]);
    O_CL[1] = tanh(CL[1]);
    O_CL[2] = tanh(CL[2]);
    O_CL[3] = tanh(CL[3]);

    O_CR[0] = tanh(CR[0]);
    O_CR[1] = tanh(CR[1]);
    O_CR[2] = tanh(CR[2]);
    O_CR[3] = tanh(CR[3]);

    O_TL[0] = tanh(TL[0]);
    O_TL[1] = tanh(TL[1]);
    O_TL[2] = tanh(TL[2]);
    O_TL[3] = tanh(TL[3]);

    O_TR[0] = tanh(TR[0]);
    O_TR[1] = tanh(TR[1]);
    O_TR[2] = tanh(TR[2]);
    O_TR[3] = tanh(TR[3]);

    //หา Max Min 
    if (O_CL[0] > MaxC) {
      MaxC = O_CL[0];
    }
    if (O_CL[0] < MinC) {
      MinC = O_CL[0];
    }
    if (O_TL[0] > MaxT) {
      MaxT = O_TL[0];
    }
    if (O_TL[0] < MinT) {
      MinT = O_TL[0];
    }
    if (O_FL[0] > MaxF) {
      MaxF = O_FL[0];
    }
    if (O_FL[0] < MinF) {
      MinF = O_FL[0];
    }


    //Map ค่า 
    for (int i = 0; i < 4; i++) {
      if (MinT == 0) {
        MaxT = 1;
      }
      M_FL[i] = mapf(O_FL[i], MinF, MaxF, 240, 275);  //60-90
      M_FR[i] = mapf(O_FR[i], MinF, MaxF, 370, 405);  //120-150

      M_CL[i] = mapf(O_CL[i], MinC, MaxC, 100, 229);  //0-20 229
      M_CR[i] = mapf(O_CR[i], MinC, MaxC, 500, 380);  //180-160

      M_TL[i] = mapf(O_TL[i], MinT, MaxT, 500, 440);  //180-160 456
      M_TR[i] = mapf(O_TR[i], MinT, MaxT, 100, 153);  //0-20
    }


    //สั่ง servo
    for (int i = 0; i < 4; i++) {

      pwm1.setPWM(i, 0, M_TR[i]);
      pwm2.setPWM(i, 0, M_TL[i]);
      // Serial.print("M_TR ");
      // Serial.print(i);
      // Serial.print(": ");
      // Serial.print(M_TR[i]);
      // Serial.print("M_Tl ");
      // Serial.print(i);
      // Serial.print(": ");
      // Serial.println(M_TL[i]);
    }
    for (int i = 0; i < 4; i++) {
      pwm1.setPWM(i + 4, 0, M_CR[i]);
      pwm2.setPWM(i + 4, 0, M_CL[i]);
      // Serial.print("M_CR ");
      // Serial.print(i);
      // Serial.print(": ");
      // Serial.println(M_CR[i]);
    }
    for (int i = 0; i < 4; i++) {
      pwm1.setPWM(i + 8, 0, M_FR[i]);
      pwm2.setPWM(i + 8, 0, M_FL[i]);
      // Serial.print("M_FR ");
      // Serial.print(i);
      // Serial.print(": ");
      // Serial.println(M_FR[i]);
    }



    delay(10);
  }
}

int mapf(double val, double in_min, double in_max, double out_min, double out_max) {
  return (val - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
int angleToPulse(int ang) {
  int pulse = map(ang, 0, 180, servoMin, servoMax);  // map angle of 0 to 180 to Servo min and Servo max

  //  Serial.print("Angle :");Serial.print(ang);
  //  Serial.print(" pulse :");Serial.println(pulse);
  return pulse;
}
